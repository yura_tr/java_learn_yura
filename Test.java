import java.util.Scanner;
import static java.lang.Math.sqrt;

public class Test {
    public static void main(String[] arg) {
        Scanner s = new Scanner(System.in);
        System.out.println("Acest program va calcula adunarea, scaderea, inmultirea si impartirea.");
        System.out.println("Introduceti primul numar.");
        int a = s.nextInt();
        System.out.println("Introduceti al doilea numar.");
        int b = s.nextInt();
        int inm,sca,adu,imp,scaInv,impInv;

        adu = a + b;
        sca = a - b;
        scaInv = b - a;
        inm = a * b;
        imp = a / b;
        impInv = b / a;


         System.out.println("________________________");
         System.out.println(a + " + " + b + " = " + adu);
         System.out.println("________________________");
          System.out.println(a + " - " + b + " = " + sca);
          System.out.println("________________________");
           System.out.println(b + " - " + a + " = " + scaInv);
           System.out.println("________________________");
            System.out.println(a + " * " + b + " = " + inm);
            System.out.println("________________________");
             System.out.println(a + " / " + b + " = " + imp);
             System.out.println("________________________");
              System.out.println(b + " / " + a + " = " + impInv);
              System.out.println("________________________");




        }
    }